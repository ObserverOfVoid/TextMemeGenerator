import {EL} from "../lib.js";

const ELF = {
  div(el, cls) {
    let div = document.createElement("div");
    if (cls) div.setAttribute("class", cls);
    el.append(div);
    return div;
  },
  sin(st, el, prop, req = false) {
    let ss = document.createElement("input");
    ss.type = "number";
    ss.required = req;
    ss.min = 0;
    el.append(ss);
    if (Object.hasOwn(st, prop)) ss.value = st[prop];
    ss.oninput = function() {
      if (!this.checkValidity()) return;
      if (this.value.length === 0) delete st[prop];
        else st[prop] = Number.parseInt(this.value);
      Ed.redraw();
    };
    return ss;
  },
  cval(v) {
    return v.length === 4 ? v.replace(/[^#]/g, "$&$&") : v;
  },
  cin(st, el, prop, req = false) {
    let
      val = Object.hasOwn(st, prop) ? st[prop] : "",
      cc = ELF.div(el, "dynh"),
      ccx = ELF.div(cc, "cin");
    ccx.style.backgroundColor = val;
    let ccc = document.createElement("input");
    ccc.type = "color";
    ccx.append(ccc);
    ccc.value = ELF.cval(val);
    ccc.oninput = function() {
      ccx.style.backgroundColor = st[prop] = cct.value = this.value;
      Ed.redraw();
    };
    let cct = document.createElement("input");
    cct.type = "text";
    cct.pattern = "#[0-9a-fA-F]{3}([0-9a-fA-F]{3})?";
    cct.required = req;
    cc.append(cct);
    cct.value = val;
    cct.oninput = function() {
      if (!this.checkValidity()) return;
      if (this.value) st[prop] = this.value;
        else delete st[prop];
      ccc.value = ELF.cval(st[prop]);
      ccx.style.backgroundColor = this.value;
      Ed.redraw();
    };
    return cc;
  },
  chin(ob, el, prop) {
    let chin = document.createElement("input");
    chin.type = "checkbox";
    el.append(chin);
    chin.checked = ob[prop];
    chin.oninput = function() {
      ob[prop] = this.checked;
      Ed.redraw();
    };
  },
  stel(ob, el, req = false) {
    let stel = ELF.div(el, "dynh dynr skit");
    ELF.sin(ob.style, stel, "size", req);
    ELF.cin(ob.style, stel, "fill", req);
    ELF.sin(ob.style, stel, "stroke", req);
    ELF.cin(ob.style, stel, "outline", req);
  },
  tero(ob, el, val) {
    ob.lines = val.split("\n");
    el.rows = Math.max(3, ob.lines.length);
  },
  tel(ob, el) {
    let tel = document.createElement("textarea");
    ELF.tero(ob, tel, tel.value = ob.content);
    el.append(tel);
    tel.oninput = function() {
      ELF.tero(ob, this, ob.content = this.value);
      Ed.redraw();
    };
  },
  pel(pos, el) {
    let pel = this.div(el, "dynh dynr skit");
    pos.forEach(po => {
      if (!("t" in po)) po.t = true;
      ELF.chin(po, pel, "t");
    });
  }
};

const  Ed = {
  valis: {
    centre: 0.5,
    center: 0.5,
    top: 0,
    bottom: 1,
    down: 0,
    up: 1
  },
  style: {
    def(ob) {
      let style = Object.create(this);
      if (ob.style) Object.assign(style, ob.style);
      ob.style = style;
    },
    font: "Verdana",
    size: 95,
    fill: "#000",
    halign: "center",
    valign: "center",
    line: 1.1,
    outline: "#FFF",
    stroke: 0
  },
  dimg: new Image(),
  g2d: EL.img.getContext("2d"),
  redraw() {
    EL.img.width = EL.iw.value;
    EL.img.height = EL.ih.value;
    
    let g2d = this.g2d, scale = EL.is.value / 100;
    g2d.setTransform(scale, 0, 0, scale, 0, 0);
    
    g2d.drawImage(this.dimg, 0, 0, this.data.width, this.data.height);
    
    this.text.forEach(tg => tg.text.forEach(tx => {
      if (tx.content.length === 0 || tx.style.size === 0) return;
      let
        st = tx.style,
        lines = tx.lines,
        lh = st.size * st.line,
        y = (this.valis[st.valign] ?? st.valign) * (lines.length - 1) * lh;
      g2d.fillStyle = st.fill;
      g2d.strokeStyle = st.outline;
      g2d.lineWidth = 2 * st.stroke;
      g2d.font = `${st.size}px ${st.font}`;
      g2d.textAlign = st.halign;
      lines.forEach(line => {
        tx.pos.forEach(po => {
          if (!po.t) return;
          if (st.stroke > 0) g2d.strokeText(line, po.x, po.y - y);
          g2d.fillText(line, po.x, po.y - y);
        });
        y -= lh;
      });
    }));
  }
};
let pars = new URLSearchParams(location.search);
Ed.temp = pars.get("temp");

EL.dla.download = `${EL.fnam.value = Ed.temp ?? "meme"}.png`;

function load(data) {
  Ed.data = data;
  document.title = `${data.names ? data.names[0] : Ed.temp} | ${document.title}`;
  EL.names.innerText = data.names ? data.names.join("\n") : Ed.temp;
  if (data.post) EL.names.href = `https://old.reddit.com/${data.post}`;
  
  EL.stl.sheet.insertRule(`#rp, #imgb:not(.hf) {width: ${100 * data.width / data.height}vh;}`);
  EL.lp.style.width = `calc(100vw - ${100 * data.width / data.height}vh)`;
  EL.iw.value = data.width;
  EL.ih.value = data.height;
  
  Ed.style.def(data);
  ELF.stel(data, EL.tes, true);
  
  (Ed.text = data.text).forEach(tg => {
    data.style.def(tg);
    
    let el = ELF.div(EL.tes, "eli stork");
    ELF.stel(tg, el);
    
    tg.text.forEach(tx => {
      tg.style.def(tx);
      
      let eel = ELF.div(el, "dynv skat");
      ELF.stel(tx, eel);
      ELF.tel(tx, eel);
      ELF.pel(tx.pos, eel);
    });
  });
  
  Ed.dimg.crossOrigin = "*";
  Ed.dimg.onload = () => Ed.redraw();
  Ed.dimg.src = data.ximg ?? (`../data/${data.img ?? Ed.temp}.png`);
};

if (pars.has("data")) load(JSON.parse(pars.get("data")));
  else fetch(`../data/${Ed.temp}.json`).then(res => res.json()).then(load);

EL.is.oninput = function() {
  EL.iw.value = Math.round(this.value * Ed.data.width / 100);
  EL.ih.value = Math.round(this.value * Ed.data.height / 100);
  Ed.redraw();
};
EL.iw.oninput = function() {
  EL.is.value = this.value * 100 / Ed.data.width;
  EL.ih.value = Math.round(this.value * Ed.data.height / Ed.data.width);
  Ed.redraw();
};
EL.ih.oninput = function() {
  EL.is.value = this.value * 100 / Ed.data.height;
  EL.iw.value = Math.round(this.value * Ed.data.width / Ed.data.height);
  Ed.redraw();
};
EL.fnam.oninput = function() {
  EL.dla.download = `${this.value}.png`;
};
EL.dl.onclick = () => {
  let uri = EL.img.toDataURL("image/png");
  EL.dla.href = uri;
  EL.dla.click();
  console.log(Ed.data);
};
EL.tf.onclick = () => EL.img.classList.toggle("hf");

onbeforeunload = () => "";